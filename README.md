Decouple WordPress installation following the technique of this website: https://deliciousbrains.com/install-wordpress-subdirectory-composer-git-submodule/

1) Remove the `.sample` to the `.env.sample` file and add your ACF PRO Key value.
2) Execute `composer install --ignore-platform-reqs`
3) Rename `wp-config-local-sample.php` to `wp-config-local.php` and make changes if it's necessary.
4) Change the theme name in the `Gruntfile.js` (line 5).
