<?php

// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
	define( 'WP_LOCAL_DEV', true );
	include( dirname( __FILE__ ) . '/wp-config-local.php' );
} else {
	define( 'WP_LOCAL_DEV', false );
	define( 'DB_NAME', '%%DB_NAME%%' );
	define( 'DB_USER', '%%DB_USER%%' );
	define( 'DB_PASSWORD', '%%DB_PASSWORD%%' );
	define( 'DB_HOST', 'localhost' );
	define( 'DB_CHARSET', 'utf8mb4' );

	// =================
	// FORZAR A USAR SSL
	// =================
	define( 'FORCE_SSL_LOGIN', true );
	define( 'FORCE_SSL_ADMIN', true );


	// =========================
	// CUSTOM WORDPRESS DATABASE
	// =========================
	define( 'WP_SITEURL',     'https://midominio.es' );
	define( 'WP_HOME',        'https://midominio.es' );

	// ===========
	// Hide errors
	// ===========
	ini_set( 'display_errors', 0 );
	define( 'WP_DEBUG_DISPLAY', false );

	// =======
	// UPDATES
	// =======
	define( 'WP_AUTO_UPDATE_CORE', false );
	define( 'DISALLOW_FILE_MODS', true );
	define( 'DISALLOW_FILE_EDIT', true );
}

// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// ==============================================================
$table_prefix = 'prowp_';


// ========================
// Custom Content Directory
// ========================
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content' );


// Set path to MU Plugins.
defined( 'WPMU_PLUGIN_DIR' ) or define( 'WPMU_PLUGIN_DIR', WP_CONTENT_DIR . '/mu-plugins' );
defined( 'WPMU_PLUGIN_URL' ) or define( 'WPMU_PLUGIN_URL', WP_CONTENT_URL . '/mu-plugins' );


// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );



// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );



// ====================
// OTHER CONFIGURATIONS
// ====================
define( 'AUTOSAVE_INTERVAL', '120' ); /* AutoSave Interval. */
define( 'WP_POST_REVISIONS', '2' );   /* Specify maximum number of Revisions. */
define( 'MEDIA_TRASH', true );		  /* Media Trash. */
define( 'EMPTY_TRASH_DAYS', '2' );	  /* Trash Days. */

// ====================
// PHP MEMORY
// ====================
define( 'WP_MEMORY_LIMIT', '256' );
define( 'WP_MAX_MEMORY_LIMIT', '512' );


// ===========
// COMPRESSION
// ===========
define( 'COMPRESS_CSS',        true );
define( 'COMPRESS_SCRIPTS',    true );
define( 'CONCATENATE_SCRIPTS', true );
define( 'ENFORCE_GZIP',        true );


// ====
// CRON
// ====
define( 'DISABLE_WP_CRON',      'true' );
define( 'ALTERNATE_WP_CRON',    'true' );
define( 'WP_CRON_LOCK_TIMEOUT', 60 );


// =======================
// BOOTSTRAP FOR WORDPRESS
// =======================
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );
require_once( ABSPATH . 'wp-settings.php' );