<?php
/*
Plugin Name: Bresca
Description: Plugin con funzionalità di base per il Web Bresca.
Version:     1.0
Author:      Mauricio Gelves
Author URI:  https://maugelves.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: brescaplugin
Domain Path: /languages
*/

// We don't want hackers in our plugin.
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// CONSTANTS
define( 'BRESCA_PATH', dirname( __FILE__ ) );
define( 'BRESCA_FILE', __FILE__ );
define( 'BRESCA_FOLDER', basename( BRESCA_PATH ) );
define( 'BRESCA_URL', plugins_url() . '/' . BRESCA_FOLDER );

/*
Set the Text Domain that will be used in the plugin
*/
define( 'BRESCA_TEXTDOMAIN', "bresca" );

include ( BRESCA_PATH . "/inc/index.php" );
