jQuery( 'document' ).ready(function($){

	frontpage = {

		init: function(){
			frontpage.initParallax();

			frontpage.initSlider();
		},
		initParallax: function(){

			// First check if there's an info block.
			if( $('.brs-info').length) {


				var ravioli = document.querySelector('.brs-info .drw-ravioli');
				var tallarin = document.querySelector('.brs-info .drw-tallarin');
				var capelletini = document.querySelector('.brs-info .drw-capelletini');

				new simpleParallax( ravioli, { overflow: true } );
				new simpleParallax( tallarin, {
												orientation: 'up left',
												overflow: true
												}
				);
				new simpleParallax( capelletini, {
												orientation: 'down left',
												overflow: true
											}
				);

			}

			// First check if there's an raffle block.
			if( $('.brs-raffle').length) {

				var baguette = document.querySelector('.brs-raffle .drw-baguette');
				var ravioli = document.querySelector('.brs-raffle .drw-ravioli');
				var pasta3 = document.querySelector('.brs-raffle .drw-pasta-03');
				var pasta4 = document.querySelector('.brs-raffle .drw-pasta-04');
				var perejil2 = document.querySelector('.brs-raffle .drw-perejil-02');


				new simpleParallax( baguette, { overflow: true } );
				new simpleParallax( ravioli, {
						orientation: 'down right',
						overflow: true
					}
				);
				new simpleParallax( pasta3, {
						orientation: 'up left',
						overflow: true
					}
				);
				new simpleParallax( pasta4, {
						orientation: 'down left',
						overflow: true
					}
				);
				new simpleParallax( perejil2, {
						overflow: true
					}
				);

			}
		},
		initSlider: function() {

			var slider = tns({
				autoplayButtonOutput: false,
				container: '.brs-reviews__items',
				controls: true,
				items: 1,
				mouseDrag: true,
				slideBy: 'page',
				autoplay: true,
				prevButton: '#slider-arrow--previous',
				nextButton: '#slider-arrow--next'
			});

		}

	};

	// Initialize the general object
	frontpage.init();
});