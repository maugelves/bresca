jQuery( 'document' ).ready(function($){
	general = {

		addEventListeners: function() {
			// Add Click Listener to the Hamburguer
			$('.hamburger').on('click', function(){
				$(this).toggleClass('is-active');
				$('.menu-collapsible').toggleClass('is-active');
			});
		},
		init: function(){
			// Add Event Listeners
			general.addEventListeners();

			// Initialize the Headroom library.
			general.initHeadRoom();
		},
		initHeadRoom: function() {
			// grab an element
			var myElement = document.querySelector(".site-header");
			var headroom  = new Headroom(myElement, {
				"offset": 205,
				"tolerance": 5
			});
			headroom.init();
		},
	};

	// Initialize the general object
	general.init();
});