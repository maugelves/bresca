jQuery( 'document' ).ready(function($){

	carta = {

		init: function(){
			carta.initParallax();
		},
		initParallax: function(){

			// First check if there's an info block.
			if( $('.brs-carta-intro').length) {

				var perejil  = document.querySelector('.brs-carta-intro .drw-perejil');
				var pasta_05 = document.querySelector('.brs-carta-intro .drw-pasta-05');
				var pasta_02 = document.querySelector('.brs-carta-intro .drw-pasta-02');
				var ravioli  = document.querySelector('.brs-carta-intro .drw-ravioli');
				var perejil_02  = document.querySelector('.brs-carta-intro .drw-perejil-02');


				new simpleParallax( pasta_02, { overflow: true } );
				new simpleParallax( pasta_05, {
						orientation: 'down right',
						overflow: true
					}
				);

				new simpleParallax( ravioli, {
						orientation: 'up left',
						overflow: true
					}
				);

				new simpleParallax( perejil_02, {
						overflow: true
					}
				);
			}

		}
	};

	// Initialize the general object
	carta.init();
});