<?php
/* Template Name: Carta regular */

get_header();

if( have_posts() ): ?>

	<section class="brs-carta">

		<?php while( have_posts() ): the_post();
			the_content();
		endwhile; ?>

	</section><!-- end .brs-carta -->

<?php endif;

get_footer();
?>