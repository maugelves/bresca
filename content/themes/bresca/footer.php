</main><!-- .site-content -->

	<footer class="site-footer">

		<div class="container">

			<header class="site-footer__header">
				<?php
					$image = get_field('brs_option_footer_logo', 'option' );
					if( $image ) {
						$args = array(
							'image' => $image,
							'class' => 'site-footer__logo',
						);
						echo mg_acf_rimage($args);
					}
				?>
			</header>

			<ul class="site-footer__sections">

				<li class="site-footer__section">
					<?php
					$args = [
						'section_title' => __('Contacto', 'bresca'),
					];
					get_extended_template_part( 'atoms/section-title', '', $args );

					the_field( 'brs_option_footer_contact', 'option' );
					?>
				</li>

				<li class="site-footer__section">
					<?php
					$args = [
						'section_title' => __('Locación', 'bresca'),
					];
					get_extended_template_part( 'atoms/section-title', '', $args );

					the_field( 'brs_option_footer_locations', 'option' );
					?>
				</li>

				<li class="site-footer__section">
					<?php
					$args = [
						'section_title' => __('Horarios', 'bresca'),
					];
					get_extended_template_part( 'atoms/section-title', '', $args );

					the_field( 'brs_option_footer_timetable', 'option' );
					?>
				</li>

				<li class="site-footer__section">
					<?php
					$args = [
						'section_title' => __('Redes', 'bresca'),
					];
					get_extended_template_part( 'atoms/section-title', '', $args );
					?>
					<p><?php the_field( 'brs_option_button_social_title', 'option' ); ?></p>

					<?php echo mg_social_links(); ?>
				</li>

			</ul>

			<div class="site-info">
				<?php printf( esc_html__( 'Web realizada por %1$s.', 'bresca' ), '<a target="_blank" href="https://maugelves.com">Mauricio Gelves</a>' ); ?>
			</div><!-- .site-info -->
		</div><!-- end .container -->

	</footer>

<?php wp_footer(); ?>

</body>
</html>
