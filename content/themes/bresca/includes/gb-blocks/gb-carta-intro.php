<?php
/**
 * Carta Intro Guteberg Block
 */
function brs_create_carta_intro_block() {
	acf_register_block_type ( [
		'category'          => 'bresca',
		'name'				=> 'carta-intro',
		'title'				=> __( 'Carta Intro', DOMAIN_NAME ),
		'description'		=> __('Bloque introductorio a la carta de Bresca', DOMAIN_NAME),
		'render_callback'	=> 'rws_gb_blocks_render_callback',
		'mode'				=> 'auto',
		'icon'				=> '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 463 463"><path d="M382 0c-12 0-22 9-24 21l-36 251c-2 12 6 23 18 26l12 3-7 130a32 32 0 0031 32c17 0 31-14 31-31V25c0-14-11-25-25-25zm10 432a16 16 0 11-32-1l7-135c0-4-2-7-6-8l-18-4c-4-1-7-5-6-10l36-251c0-4 5-8 9-8 6 0 10 4 10 10v407zM287 7a8 8 0 00-15 1l6 128h-23V8a8 8 0 00-15 0v128h-23l6-128a7 7 0 10-15-1l-8 151c0 8 4 16 11 21l9 6c2 1 4 4 4 7l-7 239a31 31 0 0031 32 31 31 0 0030-32l-7-239c0-3 2-6 4-7l9-6c7-5 11-13 11-21L287 7zm-11 159l-9 6c-7 5-11 12-11 21l7 239a16 16 0 01-15 16 16 16 0 01-16-16l7-239c0-9-4-16-11-21l-9-6c-2-1-4-4-4-7l1-8h63l1 8c0 3-2 6-4 7zM120 0c-16 0-38 8-52 45-9 23-12 50-12 67 0 22 11 42 30 54 6 3 9 8 9 14l-6 251a31 31 0 0031 32 31 31 0 0030-32l-6-251c0-6 3-11 9-14 19-12 30-32 30-55 0-16-3-43-12-66-14-37-36-45-51-45zm25 153c-10 6-16 17-16 28l6 251a16 16 0 01-15 16 16 16 0 01-16-16l6-251c0-11-6-22-16-28-14-9-23-24-23-42 0-31 12-96 49-96s48 65 48 96c0 18-9 33-23 42z"/></svg>',
		'keywords'			=> [ 'carta', 'intro', 'bresca' ],
	] );
}
add_action('acf/init', 'brs_create_carta_intro_block');