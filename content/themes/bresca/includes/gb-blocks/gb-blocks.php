<?php
/**
 * Common functions for Gutenberg Blocks.
 */

function rws_gb_blocks_render_callback( $block ) {

	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);

	// include a template part from within the "template-parts/block" folder
	if( file_exists(get_stylesheet_directory() . "/template-parts/gb-blocks/content-{$slug}.php") ) {
		include( get_stylesheet_directory() . "/template-parts/gb-blocks/content-{$slug}.php" );
	}
}
