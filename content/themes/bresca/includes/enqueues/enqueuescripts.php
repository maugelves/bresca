<?php
/*
 * Register the JS scripts
 */
function site_scripts() {

	// Load LiveReload library for development purposes
	if( defined('WP_LOCAL_DEV') && true === WP_LOCAL_DEV )
		wp_enqueue_script('livereload', get_bloginfo( 'template_url' ) . '/js/libs/livereload.js', [], '', false );

	// Register Libraries
	wp_register_script('general', get_stylesheet_directory_uri() . '/js/general.js', ['jquery'], false, true );
	wp_register_script('frontpage', get_stylesheet_directory_uri() . '/js/frontpage.js', ['jquery','parallax'], false, true );
	wp_register_script('carta', get_stylesheet_directory_uri() . '/js/carta.js', ['jquery','parallax'], false, true );
	wp_register_script('parallax', get_stylesheet_directory_uri() . '/js/libs/simpleParallax.min.js', ['jquery'], false, true ); // https://simpleparallax.com/
	wp_register_script('tinyslider', get_stylesheet_directory_uri() . '/js/libs/tiny-slider.js', [], false, true );

	/*
	 * HeadRoom Js
	 * http://wicky.nillia.ms/headroom.js/
	 */
	wp_enqueue_script('headroom', get_bloginfo( 'template_url' ) . '/js/libs/headroom.min.js', [], false, true );

	wp_enqueue_script( 'general' );

	if( is_front_page() ) {
		wp_enqueue_script( 'frontpage' );
		wp_enqueue_script( 'tinyslider' );
	}

	if( is_page_template( ['carta-con-gluten.php', 'carta-gluten-free.php'] ) ) {
		wp_enqueue_script( 'carta' );
	}
}
add_action( 'wp_enqueue_scripts', 'site_scripts' );
