<?php
/*
 * Enqueue the stylesheets
 */
function site_styles() {
	
	// Register the global styles.
	wp_register_style('style', get_stylesheet_directory_uri() . '/style.css', array(),false);
	wp_register_style('tinyslider', get_stylesheet_directory_uri() . '/js/libs/tiny-slider.css', array(),false);

	// Enqueue the styles
	wp_enqueue_style('style');
	wp_enqueue_style('tinyslider');
}
add_action( 'wp_enqueue_scripts', 'site_styles' );
add_action( 'admin_enqueue_scripts', 'site_styles' );

