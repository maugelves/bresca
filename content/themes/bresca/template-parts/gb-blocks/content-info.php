<section class="brs-info">

	<div class="container brs-info__container">

		<?php
		$args = [
			'section_title' => get_field('brs_gb_information_title' ),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>

		<div class="brs-info__b">
			<?php the_field('brs_gb_information_description') ?>
			<img class="drw-bologna" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-bologna.svg" alt="">
		</div>

		<?php
		$brs_info_img = get_field('brs_gb_information_image');
		?>
		<img class="brs-info__img" src="<?php echo $brs_info_img['sizes']['large']; ?>" alt="">
		
		<?php // Floating SVG's ?>


		<img class="drw-ravioli" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-ravioli.svg" alt="">
		<img class="drw-tallarin" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-tallarin.svg" alt="">
		<img class="drw-capelletini" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-capelletini.svg" alt="">

	</div><!-- end .container -->

</section><!-- end .brs-info -->