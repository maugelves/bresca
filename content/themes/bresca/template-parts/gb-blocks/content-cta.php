<section class="brs-cta" style="background-image: url('<?php echo esc_url( get_field('brs-gb-cta-immagine') ); ?>'); background-position: <?php echo esc_attr( get_field('brs-gb-cta-background-position') ); ?>;">

	<?php
		$args = [
			'section_title' => get_field('brs-gb-cta-title'),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );

		$args = [
			'button_text' => get_field('brs-gb-cta-button-text'),
			'button_link' => get_field('brs-gb-cta-button-link'),
		];
		get_extended_template_part( 'atoms/button', '', $args );
	?>

</section><!-- .brs-cta -->