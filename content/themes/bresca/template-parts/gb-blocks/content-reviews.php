<section class="brs-reviews">


	<div class="container brs-reviews__container">

		<img class="slider-arrow" id="slider-arrow--previous" data-slide="prev" src="<?php echo esc_html( get_stylesheet_directory_uri() . "/img/slider-arrow.svg" ); ?>" alt="">

		<?php if( have_rows('brs-reviews') ): ?>
		<ul class="brs-reviews__items">
			<?php while( have_rows( 'brs-reviews' ) ): the_row(); ?>
			<li class="brs-reviews__item">

				<?php
				$image = get_sub_field('brs-review-avatar');
				if( $image ):
					$args = array(
						'image' => $image,
						'class' => 'brs-reviews__item__avatar',
					);
					echo mg_acf_rimage($args);
				endif;
				?>

				<p class="brs-reviews__item__name"><?php echo esc_html( get_sub_field( 'brs-review-nome' ) ); ?></p>

				<?php
				$stars = get_sub_field( 'brs-review-stars' );
				?>
				<div class="brs-review-stars">
					<?php for( $x=1; $x <= 5; $x++ ):
						if( $x <= $stars ): ?>
							<img class="brs-review-star" src="<?php echo get_stylesheet_directory_uri(); ?>/img/star.svg" alt="">
						<?php else: ?>
							<img class="brs-review-star" src="<?php echo get_stylesheet_directory_uri(); ?>/img/star-transparent.svg" alt="">
						<?php endif; ?>
					<?php endfor ?>
				</div>

				<div class="brs-reviews__item__comment">
					<?php the_sub_field( 'brs-review-commento' ); ?>
				</div>

			</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>

		<img id="slider-arrow--next" data-slide="next" class="slider-arrow slider-arrow--inverted" src="<?php echo esc_html( get_stylesheet_directory_uri() . "/img/slider-arrow.svg" ); ?>" alt="">

	</div><!-- end .container -->


</section><!-- end .brs-reviews -->