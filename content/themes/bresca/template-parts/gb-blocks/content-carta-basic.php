<section class="brs-carta-basic">

	<div class="container">

		<?php
		$args = [
			'section_title' => get_field('brs-gb-carta-basic-secion-title'),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>

		<?php if( have_rows( 'brs-gb-carta-basic-group' ) ): ?>

			<?php while( have_rows( 'brs-gb-carta-basic-group' ) ): the_row(); ?>

				<?php
				$args = [
					'ribbon_text' => get_sub_field('brs-gb-carta-basic-group-name'),
					'classes'     => ['brs-cb__b'],
				];
				get_extended_template_part( 'atoms/ribbon', '', $args );
				?>

				<?php if( have_rows( 'brs-gb-carta-basic-group-dishes' ) ): ?>
					<ul class="brs-cb__dishes">
						<?php while( have_rows( 'brs-gb-carta-basic-group-dishes' ) ): the_row(); ?>
							<li class="brs-cb__dish">
								<?php
								// FEATURED IMAGE
								$image = get_sub_field('brs-gb-carta-basic-group-dish-fi');
								if( $image ) {
									$args = array(
										'image' => $image,
										'class' => 'brs-cb__dish__fi',
									);
									echo mg_acf_rimage($args);
								}
								mg_acf_rimage( $args );
								?>
								<h4 class="brs-cb__dish__h"><?php the_sub_field( 'brs-gb-carta-basic-group-dish-name' ); ?></h4>
								<p class="brs-cb__dish__price"><?php the_sub_field( 'brs-gb-carta-basic-group-dish-price' ); ?> €</p>
								<div class="brs-cb__dish__ingredients"><?php the_sub_field( 'brs-gb-carta-basic-group-dish-ingredients' ); ?></div>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

			<?php endwhile; ?>

		<?php endif; ?>

	</div><!-- end .container -->


</section><!-- end .brs-carta-individual -->