<?php

	// Variables.
	$intro_bckg_img   = get_field( 'brs_intro_header_background_image' );
	$intro_add_button = get_field( 'brs_intro_header_add_button');

?>
<section class="brs-intro" style="background-image: url(<?php echo esc_attr( $intro_bckg_img['url'] ) ?>);">

	<?php if( is_front_page() ):
			the_custom_logo();
		else: ?>
			<h2 class="brs-intro__h"><?php echo esc_html( get_field( 'brs_intro_header_title' ) );  ?></h2>
		<?php endif; ?>

	<h3 class="brs-intro__b"><?php echo esc_html( get_field( 'brs_intro_header_description' ) );  ?></h3>

	<?php if( $intro_add_button ): ?>

	<?php
		$args = [
			'button_text' => get_field('brs_intro_header_button_title' ),
			'button_link' => get_field('brs_intro_header_button_link'),
		];
		get_extended_template_part( 'atoms/button', '', $args );
	?>
	<?php endif; ?>

</section>