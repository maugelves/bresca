<section class="brs-carta-special">

	<div class="container">
		<?php
		$args = [
			'section_title' => get_field('brs_gb_carta_especial_title'),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>

		<h3 class="brs-ce__b"><?php echo esc_html( get_field('brs_gb_carta_especial_subtitle') ); ?></h3>

		<?php
		// FEATURED IMAGE 1
		$image = get_field('brs_gb_carta_especial_fi_1');
		if( $image ) {
			$args = array(
				'image' => $image,
				'class' => 'brs-ce__fi brs-ce__fi1',
			);
			echo mg_acf_rimage($args);
		}
		mg_acf_rimage( $args );


		// FEATURED IMAGE 2
		$image = get_field('brs_gb_carta_especial_fi_2');
		if( $image ) {
			$args = array(
				'image' => $image,
				'class' => 'brs-ce__fi brs-ce__fi2',
			);
			echo mg_acf_rimage($args);
		}
		mg_acf_rimage( $args );
		?>

		<?php if( have_rows( 'brs_gb_carta_especial_dishes' ) ): ?>

			<?php
			$count = count( get_field("brs_gb_carta_especial_dishes") ) / 2;
			$cont = 0;
			?>

			<ul class="brs-ce__dishes odd">
				<?php while( $cont < $count ): the_row(); ?>

					<li class="brs-ce__dish">
						<h4 class="brs-ce__dish__h"><?php echo esc_html( get_sub_field('brs_gb_carta_especial_dish_name') ); ?></h4>
						<p class="brs-ce__dish__b"><?php echo esc_html( get_sub_field('brs_gb_carta_especial_dish_ingredients') ); ?></p>
						<p class="brs-ce__dish__price"><?php echo esc_html( get_sub_field('brs_gb_carta_especial_dish_price') ); ?>€</p>
					</li>

					<?php $cont++; ?>

				<?php endwhile; ?>

			</ul><!-- end .brs-ce__dishes -->

			<ul class="brs-ce__dishes even">
				<?php while( have_rows( 'brs_gb_carta_especial_dishes' ) ): the_row(); ?>
					<li class="brs-ce__dish">
						<h4 class="brs-ce__dish__h"><?php echo esc_html( get_sub_field('brs_gb_carta_especial_dish_name') ); ?></h4>
						<p class="brs-ce__dish__b"><?php echo esc_html( get_sub_field('brs_gb_carta_especial_dish_ingredients') ); ?></p>
						<p class="brs-ce__dish__price"><?php echo esc_html( get_sub_field('brs_gb_carta_especial_dish_price') ); ?>€</p>
					</li>
				<?php endwhile; ?>
			</ul><!-- end .brs-ce__dishes -->

		<?php endif; ?>
	</div><!-- end. container -->

</section><!-- end .brs-carta-special -->