<section class="brs-raffle">

	<div class="container brs-raffle__container">

		<div class="brs-raffle-image__wrapper">
			<?php
			$image = get_field('brs-gb-raffle-image');
			if( $image ):
				$args = array(
					'image' => $image,
					'class' => 'brs-raffle__img',
				);
				echo mg_acf_rimage($args);
			endif;
			?>
		</div>

		<div class="brs-raffle__meta">

			<?php
			$args = [
				'section_title' => get_field( 'brs-gb-raffle-title' ),
			];
			get_extended_template_part( 'atoms/section-title', '', $args );

			?>

			<div class="brs-raffle__b">
				<?php echo esc_html( get_field( 'brs-gb-raffle-description' ) ); ?>
			</div>

			<?php
			$args = [
				'button_text' => get_field( 'brs-gb-raffle-button-text' ),
				'button_link' => get_field( 'brs-gb-raffle-button-link' ),
				'classes'     => ['brs-button-alt'],
			];
			get_extended_template_part( 'atoms/button', '', $args );

			?>

		</div>
	</div><!-- .container -->


	<?php // Floating SVG's ?>
	<img class="drw-baguette" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-baguette.svg" alt="">
	<img class="drw-ravioli" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-ravioli.svg" alt="">
	<img class="drw-pasta-03" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-pasta-03.svg" alt="">
	<img class="drw-pasta-04" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-pasta-04.svg" alt="">
	<img class="drw-perejil-02" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-perejil-02.svg" alt="">

</section><!-- end .brs-raffle -->