<section class="brs-carta-ad">

	<div class="container brs-cad__wrapper">

		<?php
		$args = [
			'section_title' => get_field('brs-carta-ad-title'),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>

		<?php
		$args = [
			'ribbon_text' => get_field('brs-carta-ad-description'),
			'classes'     => ['brs-cad__b'],
		];
		get_extended_template_part( 'atoms/ribbon', '', $args );
		?>

		<?php
		// FEATURED IMAGE
		$image = get_field('brs-carta-ad-fi');
		if( $image ) {
			$args = array(
				'image' => $image,
				'class' => 'brs-cad__fi',
			);
			echo mg_acf_rimage($args);
		}
		mg_acf_rimage( $args );
		?>

	</div><!-- end .container -->


</section><!-- end .brs-carta-individual -->