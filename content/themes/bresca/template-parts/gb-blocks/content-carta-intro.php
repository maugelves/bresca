<section class="brs-carta-intro">

	<div class="container">
		<?php
		$args = [
			'section_title' => get_field('brs_gb_carta_intro_title'),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>

		<p class="brs-carta-intro__b"><?php echo esc_html( get_field('brs_gb_carta_intro_description') ) ?></p>

		<?php // Drawings ?>

		<?php // Floating SVG's ?>
		<img class="drw-perejil" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-perejil.svg" alt="">
		<img class="drw-pasta-05" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-pasta-05.svg" alt="">
		<img class="drw-ravioli" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-ravioli.svg" alt="">
		<img class="drw-pasta-02" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-pasta-02.svg" alt="">
		<img class="drw-perejil-02" src="<?php echo get_stylesheet_directory_uri(); ?>/img/drawings/drw-perejil.svg" alt="">


	</div>

</section><!-- .brs-cta -->