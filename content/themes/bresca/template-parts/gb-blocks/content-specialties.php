<section class="brs-specialties">

	<div class="container">

		<?php
		$args = [
				'section_title' => get_field('gb_block_specialties_title' ),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>


		<?php if( have_rows( 'gb_block_specialties') ): ?>
			<ul class="brs-specialties-list">
			<?php while( have_rows( 'gb_block_specialties' ) ): the_row(); ?>
				<li class="brs-specialties-item">
					<?php
					$image = get_sub_field('gb_block_specialty_img');
					if( $image ):
						$args = array(
							'image' => $image,
							'class' => 'brs-specialties-item__img',
						);
						echo mg_acf_rimage( $args );
					endif; ?>
					<h4 class="brs-specialties-item__h"><?php the_sub_field( 'gb_block_specialty_nome' ); ?></h4>
					<div class="brs-specialties-item__b">
						<?php the_sub_field( 'gb_block_specialty_description' ); ?>
					</div>
				</li><!-- end .brs-specialties-item -->
			<?php endwhile; ?>
			</ul><!-- end .brs-specialties-list -->
		<?php endif; ?>

		<?php
			$args = [
				'button_text' => __( 'Ver carta', 'bresca' ),
				'button_link' => '#',
				'classes'     => ['brs-button-alt'],
			];
			get_extended_template_part( 'atoms/button', '', $args );
		?>

	</div>

</section><!-- end .brs-specialties -->