<section class="brs-carta-individual">

	<div class="container">

		<?php
		$args = [
			'section_title' => get_field('brs-carta-individual-title'),
		];
		get_extended_template_part( 'atoms/section-title', '', $args );
		?>

		<?php
		$args = [
			'ribbon_text' => get_field('brs-carta-individual-description'),
			'classes'     => ['brs-ci__b'],
		];
		get_extended_template_part( 'atoms/ribbon', '', $args );
		?>

		<?php if( have_rows( 'brs-carta-individual-platos' ) ): ?>
			<?php
				$classes= count( get_field('brs-carta-individual-platos') ) == 1 ? 'brs-ci__dishes one-dish' : 'brs-ci__dishes';
			?>
			<ul class="<?php echo esc_attr( $classes ); ?>">
			<?php while( have_rows( 'brs-carta-individual-platos' ) ): the_row(); ?>
				<li class="brs-ci__dish">
					<?php
					// FEATURED IMAGE
					$image = get_sub_field('brs-carta-individual-plato-fi');
					if( $image ) {
						$args = array(
							'image' => $image,
							'class' => 'brs-ci__fi',
						);
						echo mg_acf_rimage($args);
					}
					mg_acf_rimage( $args );
					?>
					<h4 class="brs-ci__h"><?php the_sub_field( 'brs-carta-individual-plato-nombre' ); ?></h4>
					<?php if( have_rows('brs-carta-individual-plato-ingredientes') ): ?>
					<ul class="brs-ci__ingredients">
						<?php while( have_rows('brs-carta-individual-plato-ingredientes') ): the_row(); ?>
						<li class="brs-ci__ingredient">
							<?php the_sub_field('brs-carta-individual-plato-ingrediente'); ?>
						</li>
						<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</li>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>

	</div><!-- end .container -->


</section><!-- end .brs-carta-individual -->