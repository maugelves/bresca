<?php
/**
* Atom for Section Title
*
* [section_title]    Text for the section
*/
?>
<h2 class="brs-section-title"><?php echo esc_html( $this->vars['section_title'] ); ?></h2>