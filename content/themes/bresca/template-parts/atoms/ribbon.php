<?php
/**
 * Atom for Ribbon Description
 *
 * [ribbon_text]    Text for the section
 * [classes]        Array with classes
 */

$classes = empty ( $this->vars['classes'] ) ? '' : implode( ' ', $this->vars['classes'] );
?>
<h3 class="brs-ribbon <?php echo esc_attr( $classes ); ?>"><?php echo esc_html( $this->vars['ribbon_text'] ); ?></h3>