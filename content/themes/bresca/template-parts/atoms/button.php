<?php
/**
 * Atom for buttons
 *
 * [button_text]    Text for the value attribute.
 * [button_link]    Text for the placeholder attribute.
 * [classes] Array with button classes.
 */

$classes = empty ( $this->vars['classes'] ) ? '' : implode( ' ', $this->vars['classes'] );
if( empty( $this->vars['button_link'] ) ): ?>
	<button class="brs-button <?php echo esc_attr( $classes ); ?>"><?php echo esc_html( $this->vars['button_text'] ); ?></button>
<?php else: ?>
	<button class="brs-button <?php echo esc_attr( $classes ); ?>" onclick="window.location.href = '<?php echo esc_url( $this->vars['button_link'] ); ?>';"><?php echo esc_html( $this->vars['button_text'] ); ?></button>
<?php endif; ?>