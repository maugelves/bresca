module.exports = function(grunt) {

    // NPM plugins configuration
    grunt.initConfig({
        path: 'content/themes/bresca',
        compass: {
            dev: {
                options: {
                    cssDir:         '<%= path %>/',
                    noLineComments: false,
                    outputStyle:    'expanded',
                    sassDir:        '<%= path %>/sass/',
                    specify:        '<%= path %>/sass/style.scss',
                    watch:          false
                }
            },
            dist: {
                options: {
                    cssDir:         '<%= path %>/',
                    force:          true,
                    noLineComments: false,
                    outputStyle:    'compressed',
                    sassDir:        '<%= path %>/sass/',
                    specify:        '<%= path %>/sass/style.scss',
                    watch:          false
                }
            }
        },
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')
                ]
            },
            dist: {
                src: '<%= path %>/*.css'
            }
        },
        watch:{
            options: {
                livereload: true,
            },
            sass:{
                files: ['<%= path %>/sass/**/*.scss'],
                tasks: ['compass:dev','postcss'],
                options: {
                    spawn:      false
                }
            }
        }
    });

    // Load the NPM plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-postcss');

    // Register Tasks
    grunt.registerTask('build', ['compass:dist','postcss']);
};
